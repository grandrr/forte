<?php
declare(strict_types=1);

namespace Animation;

/**
 * Class Animation
 */
class Animation
{
    /**
     * @param int $speed
     * @param string $init
     * @return array
     */
    public function animate(int $speed, string $init): array
    {
        $positions = $this->getPositions($init);

        $totalCount = strlen($init);

        $animations = [];

        $step = 0;

        while (true) {
            $exist = false;
            $animation = '';

            if ($step == 0) {
                $leftPositions = $positions['left'];
                $rightPositions = $positions['right'];
            } else {
                $this->moveLeft($leftPositions, $speed);
                $this->moveRight($rightPositions, $speed, $totalCount);
            }

            for ($i = 0; $i < $totalCount; $i++) {
                if (in_array($i, $leftPositions) || in_array($i, $rightPositions)) {
                    $animation .= 'X';
                    $exist = true;
                } else {
                    $animation .= '.';
                }
            }

            $animations[] = $animation;

            $step++;

            if (!$exist) {
                return $animations;
            }
        }
    }

    /**
     * @param string $init
     * @return array
     */
    protected function getPositions(string $init): array
    {
        $leftDirection = [];
        $rightDirection = [];
        $position = 0;

        while (true) {
            if (!($symbol = substr($init, $position, 1))) {
                break;
            }

            if ($symbol !== '.') {
                switch ($symbol) {
                    case 'L':
                        $leftDirection[] = $position;
                        break;

                    case 'R':
                        $rightDirection[] = $position;
                        break;
                }
            }

            $position++;
        }

        return ['left' => $leftDirection, 'right' => $rightDirection];
    }

    /**
     * @param array $positions
     * @param int $step
     */
    protected function moveLeft(array &$positions, int $step): void
    {
        foreach ($positions as &$position) {
            if (($position = $position - $step) <= 0) {
                unset($position);
            }
        }
    }

    /**
     * @param array $positions
     * @param int $step
     * @param int $length
     */
    protected function moveRight(array &$positions, int $step, int $length): void
    {
        foreach ($positions as &$position) {
            if (($position = $position + $step) > $length) {
                unset($position);
            }
        }
    }
}