<?php
declare(strict_types=1);

namespace Pangram;

/**
 * Class Pangram
 */
class Pangram
{
    /**
     * @var array
     */
    private $alphabet;

    /**
     * Pangram constructor.
     */
    public function __construct()
    {
        $this->alphabet = str_split('abcdefghijklmnopqrstuvwxyz');
    }

    /**
     * @param string $sentence
     * @return array
     */
    public function getMissingLetters(string $sentence): array
    {
        $foundLetters = [];

        $position = 0;

        while (true) {
            if (count($foundLetters) == 26 || !($letter = substr($sentence, $position, 1))) {
                break;
            }

            $position++;

            $letter = strtolower($letter);

            if (in_array($letter, $this->alphabet) && !in_array($letter, $foundLetters)) {
                $foundLetters[] = $letter;
            }
        }

        $diff = array_diff($this->alphabet, $foundLetters);

        sort($diff);

        return $diff;
    }
}