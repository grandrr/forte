<?php
declare(strict_types=1);

require __DIR__.'/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

/**
 * Class AnimationTest
 */
final class AnimationTest extends TestCase
{
    /**
     * @param int $speed
     * @param string $init
     * @param array $result
     *
     * @dataProvider provider
     */
    public function testAnimation(int $speed, string $init, array $result): void
    {
        $animation = new Animation\Animation();

        $this->assertEquals($animation->animate($speed, $init), $result);
    }

    /**
     * @return array
     */
    public function provider()
    {
        return [
            [
                2,
                'LRLR.LRLR',
                [
                    'XXXX.XXXX',
                    'X..X.X..X',
                    '.X.X.X.X.',
                    '.X.....X.',
                    '.........',
                ],
            ],
            [
                1,
                'RR..LRL',
                [
                    'XX..XXX',
                    '.XXX.XX',
                    '..XXX..',
                    '.X.XX..',
                    'X.X.XX.',
                    '.X...XX',
                    'X.....X',
                    '.......',
                ],
            ],
        ];
    }
}