<?php
declare(strict_types=1);

require __DIR__.'/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use \Pangram\Pangram;

/**
 * Class PangramTest
 */
final class PangramTest extends TestCase
{
    /**
     * @param string $sentence
     * @param array $missingLetters
     *
     * @dataProvider provider
     */
    public function testPangram(string $sentence, array $missingLetters): void
    {
        $pangram = new Pangram();

        $this->assertEquals($pangram->getMissingLetters($sentence), $missingLetters);
    }

    /**
     * @return array
     */
    public function provider()
    {
        return [
            [
                'A quick brown fox jumps over the lazy dog',
                [],
            ],
            [
                'A quick brown fox jumps over the',
                [
                    'd',
                    'g',
                    'l',
                    'y',
                    'z'
                ],
            ],
        ];
    }
}