<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

$animation = new Animation\Animation();

$init3 = 'LRLR.LRLR';

$animations = $animation->animate(2, $init3);

print_r($animations);