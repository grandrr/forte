<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

$anagram = new \Pangram\Pangram();

$sentance = 'A quick brown fox jumps over the lazy dog';

$missingLetters = $anagram->getMissingLetters($sentance);

print_r($missingLetters);